# IT-Buryatia-Cyberblitz

## Описание проекта

Данный проект создан для конкурса IT-Бурятия, трекер "HTML-вёрстка".

Ссылка на макет: [IT-Buryatia-Cyberblitz](https://www.figma.com/file/yNImGLv1nTcsL5YSeFIPdF/IT-Buryatia-cyberblitz?type=design&node-id=0-1&mode=design&t=ohVj5aqDWnIt9Zre-0).

Чистая верстка лежит в папке `cyberblitz-native`.

Адаптив макета cyberblitz находится в папке `cyberblitz-adaptive`.

Верстка с использованием CSS-фремворка [Bootstrap](https://getbootstrap.com/) находится в папке `cyberblitz-bootstrap`.
